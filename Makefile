build:
	dotnet run --project Builder

build-docker:
	docker build -t nuget_zarlo_builder .
	docker run --rm -v "`pwd`:`pwd`" -w "`pwd`" nuget_zarlo_builder make build

shell-docker:
	docker build -t nuget_zarlo_builder .
	docker run --rm -it -v "`pwd`:`pwd`" -w "`pwd`" nuget_zarlo_builder /bin/bash


