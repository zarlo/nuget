namespace Builder;

public class SourceConfig
{
    public string name { get; set; }
    public string uri { get; set; }
    public string? username { get; set; }
    public string? password { get; set; }
    public List<string> packages { get; set; }
}



