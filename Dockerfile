FROM mcr.microsoft.com/dotnet/sdk:7.0

RUN apt-get update -y && apt-get install build-essential -y
RUN dotnet tool install sleet -g
ENV PATH="${PATH}:/root/.dotnet/tools"